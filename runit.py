#!/usr/bin/env python

import os
from urllib import parse, request

root = "/home/cocoa/projects/LinuxInstallFest"
for filename in os.listdir(root):
    if "dw" in filename[:2]:
        with open(filename, "r") as f:
            for url in f.readlines():
                literal_url = parse.unquote(url)
                name = literal_url.split("/")[-1]
                dirname = filename.split("_")[-1]
                os.chdir(root)
                if not os.path.exists(dirname):
                    print("[INFO] Creating directory", dirname)
                    os.makedirs(dirname)
                print("[INFO] Changing into", dirname)
                os.chdir(dirname)
                print("[INFO] Starting download for", name)
                request.urlretrieve(url, name)
        os.chdir(root)

print("Done!")
